#!/bin/bash

http_proxy=$1
https_proxy=$2
no_proxy=$3


###### GIT SETUP #######
if [[ $(command -v git) ]]; then 
    echo "Setting up Git..."

    sudo -u $SUDO_USER git config --global http.proxy $http_proxy
    sudo -u $SUDO_USER git config --global https.proxy $https_proxy
else 
    echo "Git not found. Skipping proxy config for git..."
fi