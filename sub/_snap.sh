#!/bin/bash

http_proxy=$1
https_proxy=$2
no_proxy=$3


###### SNAP SETUP #######
echo "Setting up Proxy for snap..."

if [[ ! -d /etc/snap ]]; then
    mkdir /etc/snap
  fi

echo """
http_proxy=\"${http_proxy}\"
https_proxy=\"${https_proxy}\"
no_proxy=\"${no_proxy}\"
""" > /etc/snap/environment

if [[ ! -d /etc/systemd/system/snapd.service.d ]]; then
  mkdir -p /etc/systemd/system/snapd.service.d
fi

echo """
[Service]
EnvironmentFile=/etc/snap/environment
""" > /etc/systemd/system/snapd.service.d/override.conf

systemctl daemon-reload
systemctl restart snapd.service
