#!/bin/bash

http_proxy=$1
https_proxy=$2
no_proxy=$3

###### DOCKER SETUP ######
if [[ $(command -v docker) ]]; then 
    docker_home="/home/$SUDO_USER/.docker"
    docker_etc="/etc/systemd/system/docker.service.d"

    echo "Setting up Proxy for Docker"

    if [[ ! -d $docker_etc ]]; then
    mkdir /etc/systemd/system/docker.service.d
    fi

    echo """
    [Service]
    Environment=HTTP_PROXY=$http_proxy
    Environment=HTTPS_PROXY=$https_proxy
    """ > /etc/systemd/system/docker.service.d/10_docker_proxy.conf

    systemctl daemon-reload
    systemctl restart docker

    if [[ ! -d $docker_home ]]; then
    mkdir $docker_home
    fi

    echo """
    {
    \"proxies\" : {
        \"default\": {
        \"httpProxy\": \"$http_proxy\",
        \"httpsProxy\": \"$https_proxy\",
        \"noProxy\": \"$no_proxy\"
        }
    }
    }" > $docker_home/config.json
else
    echo "Docker not found. Skipping..."
fi
