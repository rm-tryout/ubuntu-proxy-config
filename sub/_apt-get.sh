#!/bin/bash

http_proxy=$1
https_proxy=$2
no_proxy=$3


###### APT-GET SETUP #######
echo "Setting up Git..."

echo """
Acquire{
  HTTP::Proxy = \"${http_proxy}\";
  HTTPS::Proxy = \"${https_proxy}\";
}""" > /etc/apt/apt.conf.d/proxy.conf
