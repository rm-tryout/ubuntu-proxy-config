#!/bin/bash
BASEDIR=$(dirname $0)

if [[ "$(id -u)"  -ne 0 ]]; then
  echo "Please run this script with root privileges"
  exit 1
fi

read -p "Please enter http proxy:  " http_proxy
read -p "Do you want to use http proxy for https as well? (y/n):  " use_http_for_https

if [[ $use_http_for_https == "y" ]]; then
  https_proxy=$http_proxy
else
  read -p "Please enter https_proxy (localhost,127.0.0.1,0.0.0.0 will automatically be added no need to add them):  " https_proxy
fi
read -p "Please enter proxy bypass:  " no_proxy

echo "HTTP_PROXY=$http_proxy"
echo "HTTPS_PROXY=$https_proxy"
echo "NO_PROXY=127.0.0.1,0.0.0.0,localhost,$no_proxy"

curl -x $http_proxy www.google.com > /dev/null

$BASEDIR/sub/_git.sh $http_proxy $https_proxy $no_proxy
$BASEDIR/sub/_docker.sh $http_proxy $https_proxy $no_proxy
$BASEDIR/sub/_apt-get.sh $http_proxy $https_proxy $no_proxy
$BASEDIR/sub/_snap.sh $http_proxy $https_proxy $no_proxy